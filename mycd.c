#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>

#define MAX_PATH 2048

int main(int argc, char *argv[]){  
  char* path;
  char* path_r;
  char* pwd;
  char buffer[MAX_PATH];

 if(argc > 2){
   printf("Numero de parametros incorrectos.\n");
 } else if (argc == 1) {
   path = malloc(sizeof(char) * strlen(getenv("HOME")));
   path[0] = '\0';
   if(path == NULL) {
     printf("Error al reservar memoria para el path.\n");
     return 1;
   }
   strcpy(path,getenv("HOME"));
   chdir(path);
   printf("%s\n",path);
 } else {
   path = malloc(sizeof(char) * strlen(argv[1]));
   path[0] = '\0';
   if(path == NULL){
     printf("Error al reservar memoria para path.\n");
     return 1;
   }
   strcpy(path,argv[1]);
   if(path[0] == '.'){
     path_r = malloc(sizeof(char) * MAX_PATH);
     path_r[0] = '\0';
     if(path_r == NULL) {
       printf("Error al reservar memoria para path_r.\n");
       return 1;
     }
     path_r = getcwd(buffer, sizeof(buffer));
     strcat(path_r,"/");
     strcat(path_r,path);
     chdir(path_r);
     pwd = malloc(sizeof(path_r));
     pwd[0] = '\0';
     if(pwd == NULL) {
       printf("Error al reservar memoria para pwd.\n");
       return 1;
     }
     pwd = getcwd(buffer, sizeof(buffer));
     printf("%s\n", pwd);
   } else {
     chdir(path); 
     pwd = malloc(sizeof(path));
     pwd[0] = '\0';
     if(pwd == NULL) {
       printf("Error al reservar memoria para pwd.\n");
       return 1;
     }
     pwd = getcwd(buffer, sizeof(buffer));
     printf("%s\n", pwd);
   }
 }
 return 0;
}
